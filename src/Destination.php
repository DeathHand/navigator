<?php

namespace navigator;

/**
 * Class Destination
 * @package navigator
 *
 * Модель окончания маршрута
 */
class Destination
{
    /**
     * @var string
     */
    protected $name = '';

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Destination
     */
    public function setName(string $name): Destination
    {
        $this->name = $name;
        return $this;
    }
}