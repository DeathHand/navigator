<?php

namespace navigator;

/**
 * Class Origin
 * @package navigator
 *
 * Модель начала маршрута
 */
class Origin
{
    /**
     * @var string
     */
    protected $name = '';

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Origin
     */
    public function setName(string $name): Origin
    {
        $this->name = $name;
        return $this;
    }
}