<?php

namespace navigator;

/**
 * Class SortException
 * @package navigator
 */
class SortException extends \Exception
{
    /**
     * @inheritdoc
     */
    public $message = 'Route is unresolvable';
}