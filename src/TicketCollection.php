<?php

namespace navigator;

/**
 * Class TicketCollection
 * @package navigator
 *
 * Коллекция билетов
 */
class TicketCollection
{
    /**
     * @var Ticket[]
     */
    protected $tickets = [];

    /**
     * @param Ticket $ticket
     */
    public function addTicket(Ticket $ticket): void
    {
        $this->tickets[] = $ticket;
    }

    /**
     * @param int $index
     */
    public function delTicket(int $index): void
    {
        unset($this->tickets[$index]);
    }

    /**
     * @param int $index
     * @return Ticket
     */
    public function getTicket(int $index): Ticket
    {
        return $this->tickets[$index];
    }

    /**
     * @return int
     */
    public function count(): int
    {
        return count($this->tickets);
    }

    /**
     * @return Ticket[]
     */
    public function getTickets(): array
    {
        return $this->tickets;
    }

    /**
     * @param Ticket[] $tickets
     */
    public function setTickets(array $tickets): void
    {
        $this->tickets = $tickets;
    }

    /**
     * @param Ticket $ticket
     * @return Ticket|null
     *
     * Производит поиск предыдущего билета, относительно переданного в $ticket
     */
    public function getPreviousOf(Ticket &$ticket): ?Ticket
    {
        foreach ($this->tickets as $index => $object) {
            if ($ticket->getOrigin()->getName() === $object->getDestination()->getName()) {
                return $object;
            }
        }

        return null;
    }

    /**
     * @param Ticket $ticket
     * @return Ticket|null
     *
     * Производит поиск последующего билета, относительно переданного в $ticket
     */
    public function getNextOf(Ticket &$ticket): ?Ticket
    {
        foreach ($this->tickets as $index => $object) {
            if ($ticket->getDestination()->getName() === $object->getOrigin()->getName()) {
                return $object;
            }
        }

        return null;
    }

    /**
     * @return Ticket|null
     *
     * Производит поиск, возврат и удаление первой точки из текущего маршрута
     */
    public function popFirst(): ?Ticket
    {
        foreach ($this->tickets as $index => $object) {
            if (!$this->getPreviousOf($object)) {
                $this->delTicket($index);
                return $object;
            }
        }

        return null;
    }

    /**
     * @param Ticket $ticket
     * @return null|object
     *
     * Производит поиск, возврат и удаление последующей точки из текущего маршрута
     * относительно переданной в $ticket
     */
    public function popNext(Ticket $ticket): ?Ticket
    {
        foreach ($this->tickets as $index => $object) {
            if ($ticket->getDestination()->getName() === $object->getOrigin()->getName()) {
                $this->delTicket($index);
                return $object;
            }
        }

        return null;
    }

    /**
     * @return TicketCollection
     * @throws SortException
     *
     * Выполняет топографическую сортировку массива билетов
     */
    public function sort(): TicketCollection
    {
        $firstTicket = $this->popFirst();

        if (!$firstTicket) {
            throw new SortException('First ticket in route not found');
        }

        $sorted[] = $firstTicket;

        while ($next = $this->popNext(end($sorted))) {
            $sorted[] = $next;
        }

        if (count($this->tickets) > 0) {
            throw new SortException();
        }

        $this->tickets = $sorted;

        return $this;
    }

    /**
     * @return string
     *
     * Сериализует содержимое коллекции в строку
     */
    public function __toString(): string
    {
        $str = '';

        foreach ($this->tickets as $ticket) {
            $str .= (string)$ticket . PHP_EOL;
        }

        return $str;
    }
}