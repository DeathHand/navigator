<?php

namespace navigator;

/**
 * Class Transport
 * @package navigator
 *
 * Модель транспорта для билета
 */
class Transport
{
    /**
     * @var string
     */
    protected $type = '';
    /**
     * @var string
     */
    protected $route = '';
    /**
     * @var string
     */
    protected $gate = '';
    /**
     * @var string
     */
    protected $seat = '';
    /**
     * @var string
     */
    protected $baggage = '';
    /**
     * @var string
     */
    protected $notes = '';

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Transport
     */
    public function setType(string $type): Transport
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getRoute(): string
    {
        return $this->route;
    }

    /**
     * @param string $route
     * @return Transport
     */
    public function setRoute(string $route): Transport
    {
        $this->route = $route;
        return $this;
    }

    /**
     * @return string
     */
    public function getGate(): string
    {
        return $this->gate;
    }

    /**
     * @param string $gate
     * @return Transport
     */
    public function setGate(string $gate): Transport
    {
        $this->gate = $gate;
        return $this;
    }

    /**
     * @return string
     */
    public function getSeat(): string
    {
        return $this->seat;
    }

    /**
     * @param string $seat
     * @return Transport
     */
    public function setSeat(string $seat): Transport
    {
        $this->seat = $seat;
        return $this;
    }

    /**
     * @return string
     */
    public function getBaggage(): string
    {
        return $this->baggage;
    }

    /**
     * @param string $baggage
     * @return Transport
     */
    public function setBaggage(string $baggage): Transport
    {
        $this->baggage = $baggage;
        return $this;
    }

    /**
     * @return string
     */
    public function getNotes(): string
    {
        return $this->notes;
    }

    /**
     * @param string $notes
     * @return Transport
     */
    public function setNotes(string $notes): Transport
    {
        $this->notes = $notes;
        return $this;
    }
}