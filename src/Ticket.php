<?php

namespace navigator;

/**
 * Class Ticket
 * @package navigator
 *
 * Модель билета
 */
class Ticket
{
    /**
     * @var Origin
     */
    protected $origin = null;
    /**
     * @var Destination
     */
    protected $destination = null;
    /**
     * @var Transport
     */
    protected $transport = null;

    /**
     * Ticket constructor.
     * @param Origin $origin
     * @param Destination $destination
     * @param Transport $transport
     */
    public function __construct(Origin &$origin, Destination &$destination, Transport &$transport)
    {
        $this->origin = $origin;
        $this->destination = $destination;
        $this->transport = $transport;
    }

    /**
     * @return Origin
     */
    public function getOrigin(): Origin
    {
        return $this->origin;
    }

    /**
     * @return Destination
     */
    public function getDestination(): Destination
    {
        return $this->destination;
    }

    /**
     * @return Transport
     */
    public function getTransport(): Transport
    {
        return $this->transport;
    }

    /**
     * @return string
     *
     * Сериализует содержимое билета в строку
     */
    public function __toString(): string
    {
        $content = '';

        if ($this->transport->getType() == 'flight') {
            $content .= "From {$this->origin->getName()} "
                . "take {$this->transport->getType()} "
                . "to {$this->destination->getName()}. "
                . "Gate {$this->transport->getGate()}. "
                . "Seat {$this->transport->getSeat()}. "
                . "Baggage {$this->transport->getBaggage()}.";
        } else {
            $content = "Take {$this->transport->getType()} ";

            if ($this->transport->getRoute()) {
                $content .= "{$this->transport->getRoute()} ";
            }

            $content .= "from {$this->origin->getName()} to {$this->destination->getName()}. ";

            if ($this->transport->getSeat()) {
                $content .= "Seat {$this->transport->getSeat()}. ";
            }
        }

        if ($this->transport->getNotes()) {
            $content .= "{$this->transport->getNotes()}. ";
        }

        return $content;
    }
}