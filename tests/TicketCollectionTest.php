<?php

use \PHPUnit\Framework\TestCase;

/**
 * Class TicketCollectionTest
 *
 * Тест коллекции биллетов
 */
class TicketCollectionTest extends TestCase
{
    /**
     * @return array
     */
    protected function getTickets(): array
    {
        return [
            (object)[
                'origin' => (object)['name' => 'Madrid'],
                'destination' => (object)['name' => 'Barcelona'],
                'transport' => (object)[
                    'type' => 'train',
                    'route' => '78A',
                    'gate' => '',
                    'seat' => '45B',
                    'baggage' => '',
                    'notes' => '',
                ],
            ],
            (object)[
                'origin' => (object)['name' => 'Stockholm'],
                'destination' => (object)['name' => 'New York JFK'],
                'transport' => (object)[
                    'type' => 'flight',
                    'route' => 'SK22',
                    'gate' => '22',
                    'seat' => '7B',
                    'baggage' => 'will be automatically transferred from your last leg',
                    'notes' => '',
                ],
            ],
            (object)[
                'origin' => (object)['name' => 'Barcelona'],
                'destination' => (object)['name' => 'Gerona Airport'],
                'transport' => (object)[
                    'type' => 'airport bus',
                    'route' => '',
                    'gate' => '',
                    'seat' => '',
                    'baggage' => '',
                    'notes' => 'No seat assignment',
                ],
            ],
            (object)[
                'origin' => (object)['name' => 'Gerona Airport'],
                'destination' => (object)['name' => 'Stockholm'],
                'transport' => (object)[
                    'type' => 'flight',
                    'route' => 'SK455',
                    'gate' => '45B',
                    'seat' => '3A',
                    'baggage' => 'drop at ticket counter 344',
                    'notes' => '',
                ],
            ],
        ];
    }

    /**
     * Тест метода сортировки
     */
    public function testCollectionSort()
    {
        $ticketCollection = new \navigator\TicketCollection();

        foreach ($this->getTickets() as $item => $object) {
            $origin = new \navigator\Origin();
            $origin->setName($object->origin->name);
            $destination = new \navigator\Destination();
            $destination->setName($object->destination->name);

            $transport = new \navigator\Transport();

            $transport->setType($object->transport->type);
            $transport->setRoute($object->transport->route);
            $transport->setGate($object->transport->gate);
            $transport->setSeat($object->transport->seat);
            $transport->setBaggage($object->transport->baggage);
            $transport->setNotes($object->transport->notes);

            $ticket = new \navigator\Ticket($origin, $destination, $transport);

            $ticketCollection->addTicket($ticket);
        }

        try {
            $ticketCollection->sort();
        } catch (\navigator\SortException $exception) {
            $this->fail($exception->getMessage());
        }

        $this->assertTrue(count($this->getTickets()) === $ticketCollection->count());
        $this->assertTrue(is_string((string)$ticketCollection));
        $this->assertSame(file_get_contents('tests/route_asset.txt'), (string)$ticketCollection);
    }
}