# Navigator

Navigator - библиотека, реализующая топологическую сортировку билетов для PHP 7.2+.

## Установка

```sh
git clone https://gitlab.com/DeathHand/navigator.git
```

## Тестирование

Для тестирования необходимо установить PHPUnit и выполнить команду phpunit в директории с проектом:

```sh
composer install
phpunit
```

## Пример использования

Для применения сортировки, необходимо заполнить коллекцию билетов объектами класса Ticket. Для создания объектов Ticket, потребуется создать объекты Origin, Destination и Transport:

```php
<?php

use navigator\Origin;
use navigator\Destination;
use navigator\Transport;
use navigator\Ticket;
use navigator\TicketCollection;
use navigator\SortException;

// Создаем инстанс коллекции билетов
$collection = new TicketCollection();

// Создаем первый билет
$origin = new Origin();
$origin->setName('Moscow');

$destination = new Destination();
$destination->setName('New York');

$transport = new Transport();

$transport->setType('flight')
    ->setRoute('SK22')
    ->setGate('22')
    ->setSeat('7B')
    ->setBaggage('drop at ticket counter 344')
    ->setNotes('No special notes');

// Добавляем первый билет в коллекцию
$collection->addTicket(new Ticket($origin, $destination, $transport));

// Создаем второй билет
$origin = new Origin();
$origin->setName('Voronezh');

$destination = new Destination();
$destination->setName('Moscow');

$transport = new Transport();

$transport->setType('train')
    ->setRoute('78A')
    ->setSeat('45B')
    ->setNotes('No special notes');

// Добавляем второй билет в коллекцию
$collection->addTicket(new Ticket($origin, $destination, $transport));

// Вызываем метод сортировки
try {
    var_dump($collection->sort()->getTickets());
} catch (SortException $exception) {
    var_dump($exception);
}
```

## Ошибки

В случае возникновения SortException при сортировке, в коллекции останутся билеты, которые не удалось отсортировать.