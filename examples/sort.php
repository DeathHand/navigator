<?php

include_once '../vendor/autoload.php';

use navigator\Origin;
use navigator\Destination;
use navigator\Transport;
use navigator\Ticket;
use navigator\TicketCollection;
use navigator\SortException;

$collection = new TicketCollection();

$origin = new Origin();
$origin->setName('Moscow');

$destination = new Destination();
$destination->setName('New York');

$transport = new Transport();

$transport->setType('flight')
    ->setRoute('SK22')
    ->setGate('22')
    ->setSeat('7B')
    ->setBaggage('drop at ticket counter 344')
    ->setNotes('No special notes');

$collection->addTicket(new Ticket($origin, $destination, $transport));

$origin = new Origin();
$origin->setName('Voronezh');

$destination = new Destination();
$destination->setName('Moscow');

$transport = new Transport();

$transport->setType('train')
    ->setRoute('78A')
    ->setSeat('45B')
    ->setNotes('No special notes');

$collection->addTicket(new Ticket($origin, $destination, $transport));

try {
    var_dump($collection->sort()->getTickets());
} catch (SortException $exception) {
    var_dump($exception);
}